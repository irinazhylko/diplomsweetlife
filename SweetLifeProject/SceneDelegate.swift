//
//  SceneDelegate.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 25.12.21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: windowScene)
        window?.windowScene = windowScene
//        window?.rootViewController = UINavigationController(rootViewController: ProfileController(nibName: String(describing: ProfileController.self), bundle: nil))
        //        window?.rootViewController = UINavigationController(rootViewController: MainController(nibName: String(describing: MainController.self), bundle: nil))
        window?.rootViewController = createTabbar()
        window?.makeKeyAndVisible()
    }
    
    func createTabbar() -> UITabBarController {
        let tabBarController = UITabBarController()
        
        let mainItem = UITabBarItem(title: "Главная", image: UIImage(systemName: "house"), tag: 0)
        let favoritesItem = UITabBarItem(title: "Избранное", image: UIImage(systemName: "heart.fill"), tag: 1)
//        let favoritesItem = UITabBarItem(title: "Избранное", image: UIImage(named: "heartFillS"), tag: 1)
        let basketItem = UITabBarItem(title: "Корзина", image: UIImage(systemName: "cart.fill"), tag: 2)
        let profileItem = UITabBarItem(title: "Профиль", image: UIImage(systemName: "person.fill"), tag: 3)
        
        //let mainVC = MainController(nibName: String(describing: MainController.self), bundle: nil)
        let mainVC = StartController(nibName: String(describing: StartController.self), bundle: nil)
//        let profileVC = ProfileController(nibName: String(describing: ProfileController.self), bundle: nil)
        let profileVC = ProfileNewVC(nibName: String(describing: ProfileNewVC.self), bundle: nil)
        let favoritesVC = FavoritesVC(nibName: String(describing: FavoritesVC.self), bundle: nil)
        let basketVC = BasketVC(nibName: String(describing: BasketVC.self), bundle: nil)
        
        
        mainVC.tabBarItem = mainItem
        favoritesVC.tabBarItem = favoritesItem
        profileVC.tabBarItem = profileItem
        basketVC.tabBarItem = basketItem
        
        let controllers: [UINavigationController] =
        [UINavigationController(rootViewController: mainVC),
         UINavigationController(rootViewController: favoritesVC),
         UINavigationController(rootViewController: basketVC),
         UINavigationController(rootViewController: profileVC)]
        
        tabBarController.viewControllers = controllers
        tabBarController.tabBar.tintColor = .black
        tabBarController.tabBar.backgroundColor = .white
        
        return tabBarController
    }
    
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    
}

