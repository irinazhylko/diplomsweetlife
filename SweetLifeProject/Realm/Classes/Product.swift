//
//  Product.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 15.01.22.
//

import Foundation
import Realm
import RealmSwift
import UIKit


class Product: Object {
    @objc dynamic var goodName: String = ""
    @objc dynamic var goodPrice: String = ""
    
    convenience init(goodName: String, goodPrice: String) {
        self.init()
        self.goodName = goodName
        self.goodPrice = goodPrice
    }
}


class ProductFavorite: Object {
    @objc dynamic var goodName: String = ""
    @objc dynamic var goodPrice: String = ""
    @objc dynamic var goodImage: Data = Data()
    
    convenience init(goodName: String, goodPrice: String, goodImage: UIImage) {
        self.init()
        self.goodName = goodName
        self.goodPrice = goodPrice
        if let image = goodImage.jpegData(compressionQuality: 1) {
            self.goodImage = image
        }
        
    }
}
