//
//  PruductInfo.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 18.01.22.
//

import Foundation
import UIKit

class ProductInfo {
    var goodName: String = ""
    var goodPrice: String = ""
    var goodDescription: String = ""
    var goodImage: Data = Data()
    
    init() {}
    
    init(goodName: String, goodPrice: String, goodDescription: String, goodImage: UIImage ) {
       // self.init()
        self.goodName = goodName
        self.goodPrice = goodPrice
        self.goodDescription = goodDescription
        if let image = goodImage.jpegData(compressionQuality: 1) {
            self.goodImage = image
        }
    }
}


//var goodsImageArrayTest: [String] = ["medovik", "napoleon", "shokoladnyj", "oblipihovyj"]
//
//var descriptionGoodArrayTest: [String] = [ "Торт медовик - это всемирно известный десерт! Пышный, сочный торт из трех нежнейших листов медового теста и с прослойкой из сметанного крема. Торт, распространённый в России и странах бывшего СССР. ","Торт наполеон - это всемирно известный десерт! Пышный, сочный торт из пяти нежнейших листов слоеного теста, щедро пропитанных сладким.","Шоколадный торт – не просто замечательный десерт. Это прекрасный повод поднять себе настроение, зарядиться бодростью и энергией", "Яркий, как само солнце, облепиховый муссовый торт. Миндальный бисквит дополняет шоколадный ганаш, а завершает это все нежнейший мусс."]
//
//var nameGoodArrayTest: [String] = ["Медовик", "Наполеон", "Шоколадный торт", "Облипиховый торт"]
