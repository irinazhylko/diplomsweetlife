//
//  ProductStructure.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 19.01.22.
//

import Foundation
import UIKit


struct ProductStructure {
    var goodName: String = ""
    var goodPrice: String = ""
    var goodDescription: String = ""
   // var goodImage: Data = Data()
    var goodImage = UIImage()
}
