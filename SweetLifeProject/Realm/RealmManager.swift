//
//  RealmManager.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 15.01.22.
//

import Foundation
import RealmSwift

class RealmManager {
    private let realm = try! Realm()
    static let shared = RealmManager()
    private init() {}
    
    func getProducts() -> [Product] {
        return Array(realm.objects(Product.self))
    }
    
    func writeProduct(product: Product) {
        try! realm.write {
            realm.add(product)
        }
    }
    
    func clearCart() {
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    func deleteFavoriteProduct(product: ProductFavorite) {
        try! realm.write {
            realm.delete(product)
            //realm.delete(realm.objects(ProductFavorite.self).filter("goodName=%@", product.goodName))
        }
    }
    
    //Записать в реалм ProductFavorite
    func getFavoriteProducts() -> [ProductFavorite] {
        return Array(realm.objects(ProductFavorite.self))
    }
    
    func writeFavoriteProduct(product: ProductFavorite) {
        try! realm.write {
            realm.add(product)
        }
    }
    
}
