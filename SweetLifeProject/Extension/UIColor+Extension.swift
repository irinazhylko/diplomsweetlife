//
//  UIColor+Extension.swift
//  Diplom
//
//  Created by Andrey Zhilko on 15.12.21.
//

import Foundation
import UIKit

extension UIColor {
    open class var mainColor: UIColor {
        return UIColor(named: "mainColor")!
    }
}


extension UIColor {
    open class var mainGreenColor: UIColor {
        return UIColor(named: "mainGreenColor")!
    }
}
