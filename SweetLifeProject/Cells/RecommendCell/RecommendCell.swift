//
//  RecommendCell.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 2.01.22.
//

import UIKit

class RecommendCell: UITableViewCell {

    @IBOutlet weak var recommendLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func setupRecommendCell() {
        recommendLabel.font = UIFont(name: "SFPro-Light", size: 18)
    }
}
