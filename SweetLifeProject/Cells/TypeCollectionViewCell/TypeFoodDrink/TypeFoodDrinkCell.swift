//
//  TypeFoodDrinkCell.swift
//  Diplom
//
//  Created by Andrey Zhilko on 28.11.21.
//

import UIKit

class TypeFoodDrinkCell: UICollectionViewCell {
    @IBOutlet weak var typeFoodImage: UIImageView!
    @IBOutlet weak var typeFoodLabel: UILabel!
    @IBOutlet weak var typeFoodView: UIView!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        typeFoodLabel.isHidden = true
    }

    func setupView() {
        super.prepareForReuse()
        typeFoodImage.layer.cornerRadius = typeFoodImage.frame.height / 2
        typeFoodView.layer.cornerRadius = 15
        //typeFoodImage.layer.borderColor = UIColor.black.cgColor
        //typeFoodImage.layer.borderWidth = 1
    }
    
    
    override func prepareForReuse() {
      super.prepareForReuse() }
}
