//
//  TypeCollectionViewCell.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 2.01.22.
//

import UIKit

class TypeCollectionViewCell: UITableViewCell {
    
    @IBOutlet weak var typeCollectionView: UICollectionView!
    
    
    var typeFoodArray: [String] = ["drinks", "hleb", "konfety", "mainCake", "pechenye", "tort"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupTypeCollectionView() {
        typeCollectionView.dataSource = self
        typeCollectionView.delegate = self
        
        let nib = UINib(nibName: String(describing: TypeFoodDrinkCell.self), bundle: nil)
        typeCollectionView.register(nib, forCellWithReuseIdentifier: String(describing: TypeFoodDrinkCell.self))
    }
}

extension TypeCollectionViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = typeCollectionView.dequeueReusableCell(withReuseIdentifier: String(describing: TypeFoodDrinkCell.self), for: indexPath)
        guard let foodCell = cell as? TypeFoodDrinkCell else { return cell }
        foodCell.typeFoodImage.image = UIImage(named: typeFoodArray[indexPath.row])
        foodCell.setupView()
        return foodCell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return typeFoodArray.count
    }
}

extension TypeCollectionViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        let width = collectionView.bounds.width
        //        let numberOfItemsPerRow: CGFloat = 1
        //        let availableWidth = width - (numberOfItemsPerRow + 10)
        //        let itemDimension = floor(availableWidth / numberOfItemsPerRow)
        //        return CGSize(width: itemDimension, height: 220)
        return CGSize(width: 80, height: 80)
        
    }
}

