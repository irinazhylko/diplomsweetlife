//
//  RecommendFoodCell.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 4.01.22.
//

import UIKit

class RecommendFoodCell: UITableViewCell {
    
    @IBOutlet weak var goodsImage: UIImageView!
    @IBOutlet weak var goodNameLabel: UILabel!
    @IBOutlet weak var goodPriceLabel: UILabel!
    @IBOutlet weak var goodView: UIView!
    @IBOutlet weak var likeViewBackground: UIView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var likeButton: UIButton!
    
    var product: Product?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupCellDescription(product: Product) {
        super.prepareForReuse()
        descriptionTextView.isEditable = false
        likeButton.contentHorizontalAlignment = .fill
        likeButton.contentVerticalAlignment = .fill
        goodsImage.layer.cornerRadius = 15
        goodView.layer.cornerRadius = 15
        likeViewBackground.layer.cornerRadius = likeViewBackground.frame.height / 2
        descriptionTextView.textColor = UIColor.mainGreenColor
        self.product = product
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse() }
    
    
//    func addInfo(productStructure: ProductStructure) {
//        goodNameLabel.text = productStructure.goodName
//        descriptionTextView.text = productStructure.goodDescription
//        goodsImage.image = productStructure.goodImage
//    }
    
    
    @IBAction func addToFavorites(_ sender: Any) {
        guard let product = product else {
            return
        }
        let favorite = ProductFavorite(goodName: product.goodName, goodPrice: product.goodPrice, goodImage: UIImage(systemName: "star")!) // в обычный продукт доьбавить фото и сюда брать фото из продукта
        if (RealmManager.shared.getFavoriteProducts().first { $0.goodName == product.goodName } != nil) {
            let alert = UIAlertController(title: "Продукт уже в избранном", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ок", style: .default))
            //present(alert, animated: true)
            //self.present(alert, animated: true)
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        } else {
            RealmManager.shared.writeFavoriteProduct(product: favorite)
        }
    }
    
}


