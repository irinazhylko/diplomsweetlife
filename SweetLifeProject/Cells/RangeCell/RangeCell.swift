//
//  RangeCell.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 2.01.22.
//

import UIKit

class RangeCell: UITableViewCell {

    @IBOutlet weak var rangeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupRangeCell() {
        rangeLabel.font = UIFont(name: "SFPro-Light", size: 25)
    }
    
    
}
