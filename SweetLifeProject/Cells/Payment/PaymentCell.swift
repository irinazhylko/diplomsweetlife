//
//  PaymentCell.swift
//  Diplom
//
//  Created by Andrey Zhilko on 26.11.21.
//

import UIKit

class PaymentCell: UITableViewCell {

    @IBOutlet weak var imagePayment: UIImageView!
    @IBOutlet weak var paymentMetodLabel: UILabel!
    @IBOutlet weak var imageChoice: UIImageView!
    @IBOutlet weak var viewCell: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setupCell() {
        viewCell.layer.cornerRadius = 8
        viewCell.layer.borderWidth = 1
    }
    
}


