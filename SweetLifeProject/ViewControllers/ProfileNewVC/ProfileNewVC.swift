//
//  ProfileNewVC.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 16.01.22.
//

import UIKit

class ProfileNewVC: UIViewController {
    @IBOutlet weak var tableViewPayment: UITableView!
    
    @IBOutlet weak var imageProfile: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Профиль"
        setupProfile()
        setupNib()
        tableViewPayment.dataSource = self
    }
    
    func setupNib() {
        let nib = UINib(nibName: String(describing: PaymentCell.self), bundle: nil)
        tableViewPayment.register(nib, forCellReuseIdentifier: String(describing: PaymentCell.self))
        
    }
    
    
    func setupProfile() {
        imageProfile.layer.cornerRadius = 15
    }
    
}

extension ProfileNewVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PaymentCell.self), for: indexPath)
        guard let paymentCell = cell as? PaymentCell else { return cell }
        paymentCell.setupCell()
        //(profile: dataArray[indexPath.row])
        return paymentCell
        
    }
}

