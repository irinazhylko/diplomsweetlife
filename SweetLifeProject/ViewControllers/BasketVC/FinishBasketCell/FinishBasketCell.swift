//
//  FinishBasketCell.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 10.01.22.
//

import UIKit

class FinishBasketCell: UITableViewCell {

    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var totalPriceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
   
    func setupFinishBasketCell () {
        buyButton.layer.cornerRadius = 10
        totalPriceLabel.font = UIFont(name: "SFPro-Light", size: 20)
        buyButton.layer.borderWidth = 1
        buyButton.layer.borderColor = UIColor.mainGreenColor.cgColor
    }
    
    
    @IBAction func goToPayOrder(_ sender: Any) {
        
    }
}
