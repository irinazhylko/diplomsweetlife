//
//  BasketCell.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 10.01.22.
//

import UIKit

class BasketCell: UITableViewCell {
    @IBOutlet weak var productBasketImage: UIImageView!
    @IBOutlet weak var nameBasketGood: UILabel!
    @IBOutlet weak var basketPriceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupBasketCell() {
        productBasketImage.layer.cornerRadius = 15
        
    }
    
}
