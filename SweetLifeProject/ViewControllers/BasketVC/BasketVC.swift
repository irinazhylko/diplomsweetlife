//
//  BasketVC.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 10.01.22.
//

import UIKit

class BasketVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var productsInBasket: [Product] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Корзина"
        tableView.dataSource = self
        registrationCell()
        tableView.separatorStyle = .none
    }
    
    
    func registrationCell() {
        let nib = UINib(nibName: String(describing: BasketCell.self), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: String(describing: BasketCell.self))
        
        let finishNib = UINib(nibName: String(describing: FinishBasketCell.self), bundle: nil)
        tableView.register(finishNib, forCellReuseIdentifier: String(describing: FinishBasketCell.self))
        
        let emptyNib = UINib(nibName: String(describing: EmptyBasketCell.self), bundle: nil)
        tableView.register(emptyNib, forCellReuseIdentifier: String(describing: EmptyBasketCell.self))
    }
}

extension BasketVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if productsInBasket.count != 0 {
            return 3
            //  productsInBasket.count
        }
       return 1
       
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if productsInBasket.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: EmptyBasketCell.self), for: indexPath)
            guard let emptyCell = cell as? EmptyBasketCell else { return cell }
            return emptyCell
        }
        if indexPath.row == 2
        //  productsInBasket.count - 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FinishBasketCell.self), for: indexPath)
            guard let finishCell = cell as? FinishBasketCell else { return cell }
            finishCell.setupFinishBasketCell()
            // проитись по всему массиву посчитать сумму всех товаров и всавить в ячейку
            return finishCell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: BasketCell.self), for: indexPath)
            guard let basketCell = cell as? BasketCell else { return cell }
            basketCell.setupBasketCell()
            return basketCell
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
      if editingStyle == .delete {
        print("Deleted")
        self.productsInBasket.remove(at: indexPath.row)
        self.tableView.deleteRows(at: [indexPath], with: .automatic)
      }
    }
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if (editingStyle == .delete) {
//            let alert = UIAlertController(title: "Удалить?", message: "Точно удаляем???", preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "Удалить", style: .destructive, handler: { _ in
//                self.productsInBasket.remove(at: indexPath.row)
//                self.tableView.reloadData()
//
//                //self.tableView.reloadSections(IndexSet(integer: indexPath.section), with: .automatic)
//            }))
//            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
//            self.present(alert, animated: true)
//        }
//    }
    
    
}

// крестик кнопой и детегатом на прошлый контролер

