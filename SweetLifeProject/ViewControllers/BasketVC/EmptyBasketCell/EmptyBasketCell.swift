//
//  EmptyBasketCell.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 16.01.22.
//

import UIKit

class EmptyBasketCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
