//
//  FavoritesVC.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 10.01.22.
//

import UIKit

class FavoritesVC: UIViewController {
    @IBOutlet weak var favoritesCollectionView: UICollectionView!
    
    var products: [ProductFavorite] = [] {
        didSet {
            favoritesCollectionView.reloadData()
//            favoritesCollectionView.reloadData()
//            favoritesCollectionView.collectionViewLayout.invalidateLayout()
//            favoritesCollectionView.layoutSubviews()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFavoritesCollectionView()
        title = "Избранное"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        products = RealmManager.shared.getFavoriteProducts()
    }
    
    
    func setupFavoritesCollectionView() {
        favoritesCollectionView.dataSource = self
        favoritesCollectionView.delegate = self
        let nib = UINib(nibName: String(describing: FavoritesCell.self), bundle: nil)
        favoritesCollectionView.register(nib, forCellWithReuseIdentifier: String(describing: FavoritesCell.self))
    }
}


extension FavoritesVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = favoritesCollectionView.dequeueReusableCell(withReuseIdentifier: String(describing: FavoritesCell.self), for: indexPath)
        guard let favoritesCell = cell as? FavoritesCell else { return cell }
        favoritesCell.setupFavoritesCell(product: products[indexPath.row])
        favoritesCell.productFavorite = products[indexPath.row]
        return favoritesCell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        products.count
    }
    
}

extension FavoritesVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let leftAndRightPaddings: CGFloat = 15.0
        let numberOfItemsPerRow: CGFloat = 2.0

        let width = (collectionView.frame.width-leftAndRightPaddings)/numberOfItemsPerRow
        return CGSize(width: width, height: 250) // You can change width and height here as pr your requirement
    }
}

extension FavoritesVC: ReloadProduct {
    func reloadProduct() {
        products = RealmManager.shared.getFavoriteProducts()
    }
    
    
}
