//
//  FavoritesCell.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 10.01.22.
//

import UIKit

protocol ReloadProduct: AnyObject {
    func reloadProduct()
}

class FavoritesCell: UICollectionViewCell {
    @IBOutlet weak var favoritesImage: UIImageView!
    @IBOutlet weak var heartImage: UIImageView!
    @IBOutlet weak var likeViewBackground: UIView!
    @IBOutlet weak var nameFavoritesGood: UILabel!
    @IBOutlet weak var favoritesPriceLabel: UILabel!
    
    var productFavorite: ProductFavorite?
    weak var productDelegate: ReloadProduct?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupFavoritesCell(product: ProductFavorite) {
        favoritesImage.layer.cornerRadius = 15
        likeViewBackground.layer.cornerRadius = likeViewBackground.frame.height / 2
        nameFavoritesGood.text = product.goodName
        favoritesPriceLabel.text = product.goodPrice
        if product.goodName == "Медовик" {
            favoritesImage.image = UIImage(named: "medovik")
        } else if product.goodName == "Наполеон" {
            favoritesImage.image = UIImage(named: "napoleon")
        } else if product.goodName == "Шоколадный торт" {
            favoritesImage.image = UIImage(named: "shokoladnyj")
        } else {
            favoritesImage.image = UIImage(named: "oblipihovyj")
            //favoritesImage.image = UIImage(data: product.goodImage)
        }
    }
    
    @IBAction func deleteFavoriteProduct(_ sender: Any) {
        guard let product = productFavorite else { return }
        RealmManager.shared.deleteFavoriteProduct(product: product)
        productDelegate?.reloadProduct()
    }
}
