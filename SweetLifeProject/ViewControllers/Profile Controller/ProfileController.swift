//
//  ProfileController.swift
//  Diplom
//
//  Created by Andrey Zhilko on 22.11.21.
//

import UIKit

class ProfileController: UIViewController {
    @IBOutlet weak var infoProfile: UIView!
    @IBOutlet weak var infoPay: UIView!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var tableViewPayment: UITableView!
    @IBOutlet weak var tableViewOrders: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupProfile()
        setupNib()
        tableViewPayment.dataSource = self
        tableViewOrders.dataSource = self
        tableViewOrders.separatorStyle = .none
    }

    func setupNib() {
        let nib = UINib(nibName: String(describing: PaymentCell.self), bundle: nil)
        tableViewPayment.register(nib, forCellReuseIdentifier: String(describing: PaymentCell.self))
        
        let nibOrder = UINib(nibName: String(describing: OrderCell.self), bundle: nil)
        tableViewOrders.register(nibOrder, forCellReuseIdentifier: String(describing: OrderCell.self))
    }
    

    func setupProfile() {
        infoProfile.layer.cornerRadius = 35
        infoPay.layer.cornerRadius = 35
        infoPay.backgroundColor = .white
        infoProfile.backgroundColor = .white
        imageProfile.layer.cornerRadius = 15
    }
    
}

extension ProfileController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewPayment {
            return 4
        } else {
//            tableView == tableViewOrders
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableViewPayment {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: PaymentCell.self), for: indexPath)
            guard let paymentCell = cell as? PaymentCell else { return cell }
            paymentCell.setupCell()
            //(profile: dataArray[indexPath.row])
            return paymentCell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: OrderCell.self), for: indexPath)
            guard let orderCell = cell as? OrderCell else { return cell }
            return orderCell
        }
     
    }
   
}
