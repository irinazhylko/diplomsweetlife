//
//  StartController.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 26.12.21.
//

import UIKit

class StartController: UIViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var productStructure: [ProductStructure] = []
    
    let recommendFoodCell = RecommendFoodCell()
   
    // var productInfo = ProductInfo()
    
    var goodsImageArray: [String] = ["medovik", "napoleon", "shokoladnyj", "oblipihovyj"]
    
    var descriptionGoodArray: [String] = [ "Торт медовик - это всемирно известный десерт! Пышный, сочный торт из трех нежнейших листов медового теста и с прослойкой из сметанного крема. Торт, распространённый в России и странах бывшего СССР. ","Торт наполеон - это всемирно известный десерт! Пышный, сочный торт из пяти нежнейших листов слоеного теста, щедро пропитанных сладким.","Шоколадный торт – не просто замечательный десерт. Это прекрасный повод поднять себе настроение, зарядиться бодростью и энергией", "Яркий, как само солнце, облепиховый муссовый торт. Миндальный бисквит дополняет шоколадный ганаш, а завершает это все нежнейший мусс."]
    
    var nameGoodArray: [String] = ["Медовик", "Наполеон", "Шоколадный торт", "Облипиховый торт"]
    
    var priceGoodArray: [String] = ["15", "15", "15", "15"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        productStructure = DataManager.shared.loadProducts()
        
        tableView.dataSource = self
        tableView.delegate = self
        setingSearchBar()
        registrationTypeCell()
        title = "Главная"
    }
    
    func setingSearchBar() {
        searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
            textfield.backgroundColor = UIColor.mainColor
            textfield.attributedPlaceholder = NSAttributedString(string: textfield.placeholder ?? "Поиск по всей еде...", attributes: [NSAttributedString.Key.foregroundColor : UIColor.systemGray])
            
            textfield.textColor = UIColor.black
            if let leftView = textfield.leftView as? UIImageView {
                leftView.image = leftView.image?.withRenderingMode(.alwaysTemplate)
                leftView.tintColor = UIColor.systemGray
            }
        }
    }
    
    
    func registrationTypeCell() {
        let nib = UINib(nibName: String(describing: TypeCollectionViewCell.self), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: String(describing: TypeCollectionViewCell.self))
        
        let nibRange = UINib(nibName: String(describing: RangeCell.self), bundle: nil)
        tableView.register(nibRange, forCellReuseIdentifier: String(describing: RangeCell.self))
        
        let nibRecommendCell = UINib(nibName: String(describing: RecommendCell.self), bundle: nil)
        tableView.register(nibRecommendCell, forCellReuseIdentifier: String(describing: RecommendCell.self))
        
        let nibRecomendFood = UINib(nibName: String(describing: RecommendFoodCell.self), bundle: nil)
        tableView.register(nibRecomendFood, forCellReuseIdentifier: String(describing: RecommendFoodCell.self))
    }
    
}

extension StartController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row > 2 {
            let descriptionVC = DescriptionProductVC(nibName: String(describing: DescriptionProductVC.self), bundle: nil)
            descriptionVC.name = nameGoodArray[indexPath.row - 3]
            descriptionVC.descriptionGood = descriptionGoodArray[indexPath.row - 3]
            descriptionVC.price = priceGoodArray[indexPath.row - 3]
            descriptionVC.imageName = goodsImageArray[indexPath.row - 3]
            navigationController?.present(descriptionVC, animated: true)

        }
        
    }
}

extension StartController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: RangeCell.self), for: indexPath)
            guard let rangeCell = cell as? RangeCell else { return cell }
            return rangeCell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TypeCollectionViewCell.self), for: indexPath)
            guard let typeCell = cell as? TypeCollectionViewCell else { return cell }
            typeCell.setupTypeCollectionView()
            return typeCell
        } else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: RecommendCell.self), for: indexPath)
            guard let recommendCell = cell as? RecommendCell else { return cell }
            return recommendCell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: RecommendFoodCell.self), for: indexPath)
            guard let recommendationCell = cell as? RecommendFoodCell else { return cell }
            recommendationCell.goodsImage.image = UIImage(named: goodsImageArray[indexPath.row - 3])
            recommendationCell.goodNameLabel.text = nameGoodArray[indexPath.row - 3]
            recommendationCell.descriptionTextView.text = descriptionGoodArray[indexPath.row - 3]
            recommendationCell.setupCellDescription(product: Product(goodName: nameGoodArray[indexPath.row - 3], goodPrice: "100"))
            
//            let product = productStructure[indexPath.row - 3]
//            recommendFoodCell.addInfo(productStructure: product)
            
            return recommendationCell
        }
    }
    
}

//extension StartController: UISearchResultsUpdating {
//    
//}


//coordinationsCell.setupCell(coordinates: coordinates[indexPath.row])

//func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//    let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NewsCell.self), for: indexPath)
//    guard let newsCell = cell as? NewsCell else { return cell }
//    if indexPath.row % 2 == 0 {
//        newsCell.infoLabel.text = "Эта ячейка четная"
//    } else {
//        newsCell.infoLabel.text = "Эта ячейка нечетная"
//    }
//    return newsCell
//}
