//
//  CompositionCell.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 18.01.22.
//

import UIKit

class CompositionCell: UITableViewCell {

    @IBOutlet weak var ingridientsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ingridientsLabel.text = "мука, масло сливочное 82,5%, вода, уксус, молоко, яйца, сливки, сахар, ванильный сахар."
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
//    func setupCell(description: String, ccal: String, ugl: String, zhyr: String, belk: String ) {
//        
//    }
    
}
