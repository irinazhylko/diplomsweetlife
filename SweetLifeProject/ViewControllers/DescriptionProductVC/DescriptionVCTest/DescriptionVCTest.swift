//
//  DescriptionVCTest.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 19.01.22.
//

import UIKit

class DescriptionVCTest: UIViewController {

    @IBOutlet weak var goodImage: UIImageView!
    @IBOutlet weak var goodName: UILabel!
    @IBOutlet weak var goodDesc: UITextView!
    
    var name = ""
    var productInfo = ProductInfo()
    var productStructure = ProductStructure()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        goodName.text = name
        updateUI()
    }

    func setupInfo() {
        goodName.text = productInfo.goodName
        goodDesc.text = productInfo.goodDescription
    }
    
    func updateUI() {
        title = productStructure.goodName
        goodName.text = productStructure.goodName
        goodDesc.text = productStructure.goodDescription
        goodImage.image = productStructure.goodImage
    }
}
