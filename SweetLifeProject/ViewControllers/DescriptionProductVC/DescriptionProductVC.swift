//
//  DescriptionProductVC.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 4.01.22.
//

import UIKit

class DescriptionProductVC: UIViewController  {
    @IBOutlet weak var tableView: UITableView!
    
    var productInfo = ProductInfo()
    
    var name = ""
    var imageName = ""
    var descriptionGood = ""
    var price = ""
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        registrationDescCell()
    }
    
    func registrationDescCell() {
        let nib = UINib(nibName: String(describing: HeaderImageCell.self), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: String(describing: HeaderImageCell.self))
        
        let nibDesc = UINib(nibName: String(describing: NameDescCell.self), bundle: nil)
        tableView.register(nibDesc, forCellReuseIdentifier: String(describing: NameDescCell.self))
        
        let nibCompos = UINib(nibName: String(describing: CompositionCell.self), bundle: nil)
        tableView.register(nibCompos, forCellReuseIdentifier: String(describing: CompositionCell.self))
    }
}

extension DescriptionProductVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: HeaderImageCell.self), for: indexPath)
            guard let photoCell = cell as? HeaderImageCell else { return cell }
            photoCell.setupImage(imageName: imageName)
            return photoCell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NameDescCell.self), for: indexPath)
            guard let nameCell = cell as? NameDescCell else { return cell }
            nameCell.setupCell(name: name, description: descriptionGood)
            return nameCell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CompositionCell.self), for: indexPath)
            guard let compositionCell = cell as? CompositionCell else { return cell }
            
            return compositionCell
        }
    }
}


