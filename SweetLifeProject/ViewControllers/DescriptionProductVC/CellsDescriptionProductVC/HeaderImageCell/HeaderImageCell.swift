//
//  HeaderImageCell.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 4.01.22.
//

import UIKit

class HeaderImageCell: UITableViewCell {
    @IBOutlet weak var height: NSLayoutConstraint!
    @IBOutlet weak var imageViewCell: UIImageView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setupImage(imageName: String) {
        imageViewCell.image = UIImage(named: "\(imageName)")
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
