//
//  NameDescCell.swift
//  SweetLifeProject
//
//  Created by Andrey Zhilko on 18.01.22.
//

import UIKit

class NameDescCell: UITableViewCell {

    @IBOutlet weak var goodNameLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCell(name: String, description: String) {
        goodNameLabel.text = "Торт " + "\(name)"
        descriptionTextView.text = description
        
    }
    
}
